import { Component, OnInit } from '@angular/core';
import { EventManager } from '@angular/platform-browser';
import {NgForm} from '@angular/forms';
import { FormGroup, FormControl } from '@angular/forms';
import {SignupService} from './signupservice';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [SignupService]
})
export class SignupComponent implements OnInit {

  api: any[] = null;
  public signupForm:FormGroup;
  public controlsdata:any;
  constructor(
    public SignupService:SignupService
  ) {}

  ngOnInit() {
    this.signupForm = new FormGroup({
      username: new FormControl(''),
      password: new FormControl(''),
      mobile: new FormControl(''),
      name: new FormControl(''),
      email: new FormControl(''),
    
   });
  }
   signup() {
    this.SignupService.getRequest(this.signupForm.value,'')
    .subscribe(data => { 
      console.log(data);
    });

  }

}





  