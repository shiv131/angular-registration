import { Component, OnInit } from '@angular/core';
import { EventManager } from '@angular/platform-browser';
import {NgForm} from '@angular/forms';
import { FormGroup, FormControl } from '@angular/forms';
import {signinService} from './signinservice';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css'],
  providers:[signinService]
})
export class SigninComponent implements OnInit {
  api: any[] = null;
  public profileForm:FormGroup;
  public controlsdata:any;
  constructor(
    public signinService:signinService
  ) {}
  ngOnInit() {
    
    this.profileForm = new FormGroup({
       email: new FormControl(''),
       password: new FormControl(''),
     
    });
   
   }
  
  login() {
   this.signinService.getRequest(this.profileForm.value,'')
   .subscribe(data => { 
     console.log(data);
   });
 }
 }
 

